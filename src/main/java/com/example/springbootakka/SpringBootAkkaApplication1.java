package com.example.springbootakka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;


public final class SpringBootAkkaApplication1 {
 
    public static ActorRef espadachin1;
    public static ActorRef espadachin2;
    
    public static ActorRef herrero;
    public static ActorRef minero;
    
    public static ActorSystem actorSystem;
 
    private static final long TIEMPO_ESPERA = 5000;
 
    public static void main(String[] args) throws InterruptedException {
    	
        actorSystem = ActorSystem.create("ActorSystem");
        
        espadachin1 = actorSystem.actorOf(Props.create(Espadachin.class), "espadachin1");
        espadachin2 = actorSystem.actorOf(Props.create(Espadachin.class), "espadachin2");
        
        herrero = actorSystem.actorOf(Props.create(Herrero.class), "herrero");
        minero = actorSystem.actorOf(Props.create(Minero.class), "minero");
        
        /**
         * Metodo tell 
         * 
         * El método SEND ajusta un indicador normal y proporciona la referencia del actor interno como remitente. 
         * Esto permite recibir la respuesta en la última línea.
         * 
         */
        espadachin1.tell(Espadachin.Mensaje.ESPADA_ROTA, ActorRef.noSender());
        Thread.sleep(TIEMPO_ESPERA);
        // tambien podemos enviar un mensaje como
        //espadachin2.tell(Espadachin.Mensaje.ESPADA_ROTA, ActorRef.noSender());
        espadachin2.tell("ESPADA_ROTA", ActorRef.noSender());
    }
}
